import static org.junit.Assert.*;

import org.junit.Test;

public class CircleTest {

	@Test
	public void testConstructor() {
		circle c = new circle(5);
		
		double expectedRadius = 5;
		double actualRadius = c.getRadius();
		
		//	assertEquals(x,y,0.01);
		// x = expected output
		// y = actual output
		// 0.01 = just use 0.01
		// OPTION 1: WRite it like this:
		// 		assertEquals(5, c.getRadius(), 0.01);	
		assertEquals(expectedRadius, actualRadius, 0.01);
		
		
	}
	
	
	@Test
	public void testAreaFunction() {
		circle c = new circle(5);
		double expectedArea = 78.5;
		double actualArea = c.getArea();
		assertEquals(78.5, actualArea, 0.1);
	}
	
	
	@Test
	public void testCircumFunction() {
		circle c = new circle(5);
		double expectedC = Math.PI * 2 * 5;
		double actualC = c.getCircumference();
		assertEquals(expectedC, actualC, 0.01);
	}
	
	
	
	@Test
	public void testDiameterFunction() {
		circle c = new circle(5);
		double expectedDiameter = 10;
		double actualDiameter = c.getDiameter();
		assertEquals(expectedDiameter, actualDiameter, 0.01);
	}
	
}



